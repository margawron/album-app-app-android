package com.github.margawron

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.MediaStore
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.github.margawron.data.auth.AuthManager
import com.github.margawron.viewmodels.FileDisplayViewModel
import com.github.margawron.databind.fragments.MediaViewFragment
import com.github.margawron.databind.fragments.SettingsFragment
import com.github.margawron.databinding.FileDisplayActivityBinding
import com.google.android.material.navigation.NavigationBarView
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class FileDisplayActivity : AppCompatActivity() {

    companion object{
        const val FRAGMENT_ID_KEY = "FRAGMENT_ID"
    }

    private val viewModel by viewModels<FileDisplayViewModel>()

    @Inject
    lateinit var authManager: AuthManager

    private val mediaViewFragment = MediaViewFragment()
    private val settingsFragment = SettingsFragment()

    private val requestPermission: (String) -> Unit = { permission: String ->
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted ->
            if (!isGranted) {
                this.finish()
            }
        }.launch(permission)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (authManager.getToken() == null) {
            val loginIntent = Intent(this, LoginActivity::class.java)
            loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(loginIntent)
        } else {
            val binding: FileDisplayActivityBinding =
                DataBindingUtil.setContentView(this, R.layout.file_display_activity)

            val application = (application as MainApplication)

            // Request permission
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ) == PackageManager.PERMISSION_DENIED
            ) {
                requestPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
            }


            contentResolver.registerContentObserver(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                true,
                application.imageObserver
            )

            contentResolver.registerContentObserver(
                MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                true,
                application.videoObserver
            )
            contentResolver.registerContentObserver(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                true,
                application.audioObserver
            )


            binding.lifecycleOwner = this
            val fragmentId = savedInstanceState?.getInt(FRAGMENT_ID_KEY) ?: R.id.ic_media_view
            selectFragmentById(fragmentId)

            val bottomNav: NavigationBarView = binding.bottomNavigation
            bottomNav.setOnItemSelectedListener {
                savedInstanceState?.putInt(FRAGMENT_ID_KEY, it.itemId)
                selectFragmentById(it.itemId)
                true
            }

        }
    }

    private fun selectFragmentById(fragmentId: Int) {
        when(fragmentId){
            R.id.ic_media_view -> changeCurrentlyDisplayedFragment(mediaViewFragment)
            R.id.ic_settings -> changeCurrentlyDisplayedFragment(settingsFragment)
        }
    }

    private fun changeCurrentlyDisplayedFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().apply {
            replace(R.id.fragment_wrapper, fragment)
            commit()
        }
    }
}