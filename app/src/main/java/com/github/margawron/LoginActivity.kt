package com.github.margawron

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.fasterxml.jackson.databind.ObjectMapper
import com.github.margawron.data.auth.AuthManager
import com.github.margawron.viewmodels.LoginViewModel
import com.github.margawron.databinding.LoginActivityBinding
import com.github.margawron.service.MediaChangeService
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val viewModel by viewModels<LoginViewModel>()

    @Inject
    lateinit var authManager: AuthManager
    @Inject
    lateinit var objectMapper: ObjectMapper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: LoginActivityBinding = DataBindingUtil.setContentView(this, R.layout.login_activity)
        binding.lifecycleOwner = this
        binding.vm = viewModel
        setupOnLoginClick()
    }

    private fun setupOnLoginClick() {
        viewModel.onLoginButtonClickedCallback = {
            it.launch {
                withContext(Dispatchers.IO) {
                    val response = authManager.loginUser(viewModel.login, viewModel.password)
                    when {
                        response.isSuccessful -> {
                            viewModel.checkIfShouldRememberPassword()
                            Log.d("setupOnLoginClick", "Token ${response.body()?.token}")
                            val serviceIntent =
                                Intent(this@LoginActivity, MediaChangeService::class.java)
                            startForegroundService(serviceIntent)
                            val mainViewIntent =
                                Intent(this@LoginActivity, FileDisplayActivity::class.java)
                            startActivity(mainViewIntent)

                        }
                        !response.isSuccessful -> {
                            val error = response.errorBody()?.string()
                            Log.d("setupOnLoginClick", "Error $error")
                            val errorMessage = error?.let {
                                val json = objectMapper.readTree(it)
                                json["errorMessage"].asText()
                            } ?: "Server error"
                            withContext(Dispatchers.Main) {
                                Toast.makeText(this@LoginActivity, errorMessage, Toast.LENGTH_SHORT)
                                    .show()
                            }
                        }
                    }
                }

            }
        }
    }
}