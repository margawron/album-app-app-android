package com.github.margawron

import android.app.Application
import androidx.hilt.work.HiltWorkerFactory
import androidx.work.Configuration
import com.github.margawron.data.filechanges.FileChangesRepository
import com.github.margawron.data.filechanges.FileKind
import com.github.margawron.fileobserver.UriObserver
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


@HiltAndroidApp
class MainApplication : Application(), Configuration.Provider {
    lateinit var imageObserver: UriObserver
    lateinit var videoObserver: UriObserver
    lateinit var audioObserver: UriObserver

    @Inject
    lateinit var fileChangesRepository: FileChangesRepository

    @Inject
    lateinit var workFactory: HiltWorkerFactory

    override fun onCreate() {
        super.onCreate()
        val scope = CoroutineScope(Dispatchers.IO)
        imageObserver = UriObserver(FileKind.IMAGE, fileChangesRepository, scope)
        videoObserver = UriObserver(FileKind.VIDEO, fileChangesRepository, scope)
        audioObserver = UriObserver(FileKind.AUDIO, fileChangesRepository, scope)
    }

    override fun getWorkManagerConfiguration(): Configuration {
        return Configuration.Builder()
            .setWorkerFactory(workFactory)
            .build()
    }

}