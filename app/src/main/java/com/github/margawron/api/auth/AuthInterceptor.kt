package com.github.margawron.api.auth

import com.github.margawron.data.auth.AuthManager
import dagger.Lazy
import okhttp3.Interceptor
import okhttp3.Response
import java.time.Instant
import javax.inject.Inject


class AuthInterceptor @Inject constructor(
    private val lazyAuthManager: Lazy<AuthManager>
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val requestBuilder = request.newBuilder()
        val authManager = lazyAuthManager.get()

        val token = authManager.getToken()
        val tokenExpirationInstant = authManager.getTokenExpirationInstant()
        val isTokenExpired = tokenExpirationInstant?.isBefore(Instant.now()) ?: true
        if (token != null && !isTokenExpired){
            requestBuilder.addHeader("Authorization", "Bearer $token")
        }
        return chain.proceed(requestBuilder.build())
    }
}