package com.github.margawron.api.auth

class TokenRequest(
    val username: String,
    val password: String
)