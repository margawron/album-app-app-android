package com.github.margawron.api.auth

import java.time.Instant

data class TokenResponse(
    val expirationDate: Instant,
    val token: String,
    val tokenType: String
)