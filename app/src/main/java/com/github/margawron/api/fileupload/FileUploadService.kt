package com.github.margawron.api.fileupload

import okhttp3.MultipartBody
import retrofit2.Response
import retrofit2.http.Multipart
import retrofit2.http.POST
import retrofit2.http.Part

interface FileUploadService {

    @POST("media/upload")
    @Multipart
    suspend fun uploadMedia(@Part filePart: MultipartBody.Part): Response<MediaDto>

}