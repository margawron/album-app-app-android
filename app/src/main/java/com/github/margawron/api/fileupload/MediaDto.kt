package com.github.margawron.api.fileupload

data class MediaDto(
    val id: Long,
    val mimeType: String,
    val name: String,
    val ownerId: Long,
    val tags: List<Long>
)
