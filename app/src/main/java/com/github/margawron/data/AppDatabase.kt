package com.github.margawron.data

import android.content.Context
import androidx.room.*
import com.github.margawron.data.filechanges.FileChange
import com.github.margawron.data.filechanges.FileChangeStatusConverter
import com.github.margawron.data.filechanges.FileChangesDao
import com.github.margawron.data.filechanges.FileKindConverter
import com.github.margawron.data.util.InstantConverter

@Database(
    entities = [
        FileChange::class
    ],
    version = 2
)
@TypeConverters(value = [
    InstantConverter::class,
    FileChangeStatusConverter::class,
    FileKindConverter::class
])
abstract class AppDatabase : RoomDatabase() {
    abstract fun fileChangesDao(): FileChangesDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also{ instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room
                .databaseBuilder(context, AppDatabase::class.java, "media-backup-manager")
                .fallbackToDestructiveMigration()
                .build()
        }
    }

}