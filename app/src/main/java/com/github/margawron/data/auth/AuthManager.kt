package com.github.margawron.data.auth

import android.util.Log
import com.github.margawron.api.auth.AuthService
import com.github.margawron.api.auth.TokenRequest
import com.github.margawron.api.auth.TokenResponse
import dagger.Provides
import retrofit2.Response
import java.time.Instant
import javax.inject.Inject
import javax.inject.Singleton

class AuthManager(
    private val authService: AuthService
){
    private var token: String? = null
    private var tokenExpirationInstant: Instant? = null

    suspend fun loginUser(username: String, password: String): Response<TokenResponse>{
        val tokenResponse: Response<TokenResponse> = authService.login(TokenRequest(username, password))
        if (!tokenResponse.isSuccessful){
            Log.d("loginUser", "Login unsuccessful")
            return tokenResponse;
        }
        val body = tokenResponse.body()!!
        token = body.token
        tokenExpirationInstant = body.expirationDate
        return tokenResponse
    }

    fun logout(){
        token = null
        tokenExpirationInstant = null
    }

    fun getToken() = token

    fun getTokenExpirationInstant() = tokenExpirationInstant
}