package com.github.margawron.data.filechanges

import androidx.room.*
import com.github.margawron.data.util.InstantConverter
import java.time.Instant

@Entity(
    indices = [
        Index(
            value = ["flc_android_identity"],
            name = "FileChangeAndroidIdentityIndex",
            unique = true
        )
    ]
)

class FileChange (
    @PrimaryKey
    @ColumnInfo(name = "flc_id")
    val id: Long?,

    @ColumnInfo(name = "flc_uri")
    val uri: String,

    @ColumnInfo(name = "flc_android_identity")
    val androidIdentity: Long?,

    @ColumnInfo(name = "flc_status")
    var status: FileChangeStatus,

    @ColumnInfo(name = "flc_kind")
    val kind: FileKind,

    @ColumnInfo(name = "flc_creation_instant")
    val creationInstant: Instant,

    @ColumnInfo(name = "flc_update_instant")
    var updateInstant: Instant
)