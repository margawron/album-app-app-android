package com.github.margawron.data.filechanges

enum class FileChangeStatus {
    UPLOADED,
    PENDING
}