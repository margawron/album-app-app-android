package com.github.margawron.data.filechanges

import androidx.room.TypeConverter
import java.lang.IllegalStateException
import java.lang.RuntimeException

class FileChangeStatusConverter {

    @TypeConverter
    fun fromFileChangeStatus(status: FileChangeStatus): String {
        return when(status){
            FileChangeStatus.UPLOADED -> "U"
            FileChangeStatus.PENDING -> "P"
        }
    }


    @TypeConverter
    fun fromString(string: String): FileChangeStatus {
        return when(string){
            "U" -> FileChangeStatus.UPLOADED
            "P" -> FileChangeStatus.PENDING
            else -> throw IllegalStateException("Invalid enum mapping")
        }
    }
}