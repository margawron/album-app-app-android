package com.github.margawron.data.filechanges

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface FileChangesDao {

    @Insert
    suspend fun insert(fileChange: FileChange): Long

    @Update
    suspend fun update(fileChange: FileChange)

    @Delete
    suspend fun delete(fileChange: FileChange)

    @Query("SELECT * FROM FileChange WHERE flc_id = :id")
    suspend fun findById(id: Long): FileChange?

    @Query("SELECT * FROM FileChange WHERE flc_android_identity = :androidId")
    suspend fun findByAndroidId(androidId: Long): FileChange?

    @Query("SELECT COUNT(*) FROM FileChange WHERE flc_status = :status")
    fun getCountForStatus(status: FileChangeStatus): LiveData<Long>

    @Query("SELECT * FROM FileChange WHERE flc_status = :status ORDER BY flc_id DESC LIMIT 1")
    fun getLastFileChangeForStatus(status: FileChangeStatus): LiveData<FileChange?>

    @Query("SELECT * FROM FileChange ORDER BY flc_id DESC")
    fun getFileChangesDescending(): LiveData<List<FileChange>>

    @Query("SELECT * FROM FileChange WHERE flc_status = :status")
    suspend fun findAllByStatus(status: FileChangeStatus): List<FileChange>
}