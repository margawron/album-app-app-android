package com.github.margawron.data.filechanges

import android.net.Uri
import android.util.Log
import androidx.lifecycle.LiveData
import com.github.margawron.fileobserver.FileChangeFlag
import java.time.Instant
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileChangesRepository @Inject constructor(
    private val fileChangesDao: FileChangesDao
) {

    suspend fun findFileChangeById(id: Long): FileChange? = fileChangesDao.findById(id)

    suspend fun findAllByStatus(status: FileChangeStatus): List<FileChange> = fileChangesDao.findAllByStatus(status)

    suspend fun update(fileChange: FileChange) = fileChangesDao.update(fileChange)

    suspend fun delete(fileChange: FileChange) = fileChangesDao.delete(fileChange)

    fun getFileChanges(): LiveData<List<FileChange>> = fileChangesDao.getFileChangesDescending()

    fun getUploadedItemsCount(): LiveData<Long> =
        fileChangesDao.getCountForStatus(FileChangeStatus.UPLOADED)

    fun getPendingItemsCount(): LiveData<Long> =
        fileChangesDao.getCountForStatus(FileChangeStatus.PENDING)

    fun getLatestUpdate(): LiveData<FileChange?> = fileChangesDao.getLastFileChangeForStatus(FileChangeStatus.UPLOADED)

    suspend fun registerFileChange(uri: Uri, fileKind: FileKind, fileChangeFlags: Set<FileChangeFlag>) {
        Log.d(
            "registerFileChange",
            "File changed $uri with flags ${fileChangeFlags.joinToString()}"
        )
        when {
            fileChangeFlags.contains(FileChangeFlag.INSERT) -> registerInsertEvent(uri, fileKind)
            fileChangeFlags.contains(FileChangeFlag.UPDATE) -> registerUpdateEvent(uri)
            fileChangeFlags.contains(FileChangeFlag.DELETE) -> registerDeleteEvent(uri)
        }
    }

    private suspend fun registerInsertEvent(uri: Uri, fileKind: FileKind) {
        val androidIdentity = uri.lastPathSegment?.toLong()
        val fileChange = FileChange(
            id = null,
            uri = uri.toString(),
            androidIdentity = androidIdentity,
            status = FileChangeStatus.PENDING,
            kind = fileKind,
            creationInstant = Instant.now(),
            updateInstant = Instant.now()
        )
        fileChangesDao.insert(fileChange)
    }

    private suspend fun registerUpdateEvent(uri: Uri) {
        val androidIdentity = uri.lastPathSegment?.toLong()
        androidIdentity?.let {
            val fileChange = fileChangesDao.findByAndroidId(it)
            fileChange?.let { change ->
                change.updateInstant = Instant.now()
                fileChangesDao.update(change)
            }
        }
    }

    private suspend fun registerDeleteEvent(uri: Uri) {
        val androidIdentity = uri.lastPathSegment?.toLong()
        androidIdentity?.let {
            val fileChange = fileChangesDao.findByAndroidId(it)
            fileChange?.let { change ->
                fileChangesDao.delete(change)
            }
        }
    }
}