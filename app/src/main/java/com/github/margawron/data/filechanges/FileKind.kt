package com.github.margawron.data.filechanges

enum class FileKind {
    IMAGE,
    VIDEO,
    AUDIO
}