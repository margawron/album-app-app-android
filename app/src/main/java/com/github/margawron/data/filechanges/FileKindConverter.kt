package com.github.margawron.data.filechanges

import androidx.room.TypeConverter
import java.lang.IllegalStateException

class FileKindConverter {

    @TypeConverter
    fun fromFileKind(status: FileKind): String {
        return when(status){
            FileKind.IMAGE -> "I"
            FileKind.AUDIO -> "A"
            FileKind.VIDEO -> "V"
        }
    }


    @TypeConverter
    fun fromString(string: String): FileKind {
        return when(string){
            "I" -> FileKind.IMAGE
            "A" -> FileKind.AUDIO
            "V" -> FileKind.VIDEO
            else -> throw IllegalStateException("Invalid enum mapping")
        }
    }


}