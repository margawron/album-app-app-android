package com.github.margawron.data.fileupload

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.util.Log
import android.widget.Toast
import androidx.work.*
import com.github.margawron.api.fileupload.FileUploadService
import com.github.margawron.data.filechanges.FileChange
import com.github.margawron.data.filechanges.FileChangeStatus
import com.github.margawron.data.filechanges.FileChangesRepository
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.asExecutor
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import okhttp3.*
import java.lang.IllegalStateException
import java.time.Duration
import java.time.Instant
import java.time.temporal.ChronoUnit
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FileUploadRepository @Inject constructor(
    private val fileUploadService: FileUploadService,
    private val fileChangesRepository: FileChangesRepository,
    @ApplicationContext private val context: Context
) {

    companion object {
        const val FORCE_UPLOAD_WORK_TAG = "FORCE_UPLOAD_WORK_TAG"
        const val PERIODIC_UPLOAD_WORK_TAG = "PERIODIC_UPLOAD_WORK_TAG"
    }

    private val contentResolver: ContentResolver = context.contentResolver

    private var forceUploadWorkRequest: OneTimeWorkRequest? = null
    private var periodicUploadWorkRequest: PeriodicWorkRequest = setupPeriodicUploadWorker()

    suspend fun uploadAllFiles() {
        val fileChanges = fileChangesRepository.findAllByStatus(FileChangeStatus.PENDING)
        fileChanges.forEach {
            uploadFile(it.id!!)
        }
    }

    suspend fun uploadFile(fileChangeId: Long) {
        val fileChange = fileChangesRepository.findFileChangeById(fileChangeId)
        fileChange?.let { change ->
            val requestBody = getRequestBody(change)
            requestBody?.let { request ->
                val part = MultipartBody.Part.createFormData("file", fileChange.uri, request)
                val uploadResponse = fileUploadService.uploadMedia(part)
                if (!uploadResponse.isSuccessful) {
                    Log.d(
                        "uploadFile",
                        "Upload failed with message ${
                            uploadResponse.errorBody()?.string() ?: "Unknown error"
                        }"
                    )
                    Toast.makeText(context, "Upload failed", Toast.LENGTH_SHORT).show()
                    return
                }
                change.updateInstant = Instant.now()
                change.status = FileChangeStatus.UPLOADED
                fileChangesRepository.update(change)
            }
        }
    }

    private suspend fun getRequestBody(change: FileChange): RequestBody? {
        val uri = Uri.parse(change.uri)
        val mimeType: String = contentResolver.getType(uri)!!
        var data: ByteArray? = null
        try {
            data =
                contentResolver.openAssetFileDescriptor(uri, "r")!!.createInputStream().readBytes()
        } catch (e: IllegalStateException) {
            fileChangesRepository.delete(change)
        }
        Log.d("getImageRequestBody", "mimeType $mimeType")
        val requestBody = data?.let {
            RequestBody.create(MediaType.parse(mimeType), data)
        }
        return requestBody
    }

    suspend fun setupForceUploadWork() {
        if (forceUploadWorkRequest == null) {
            forceUploadWorkRequest = OneTimeWorkRequestBuilder<UploadImageWorker>()
                .addTag(FORCE_UPLOAD_WORK_TAG)
                .build()

            WorkManager.getInstance(context).enqueueUniqueWork(
                FORCE_UPLOAD_WORK_TAG,
                ExistingWorkPolicy.KEEP,
                forceUploadWorkRequest!!
            )
            setupNextOnForceUploadWorkerChangeListener()
        } else {
            withContext(Dispatchers.Main){
                Toast.makeText(context, "Image upload was queued", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun setupNextOnForceUploadWorkerChangeListener(){
        val workInfoFuture =
            WorkManager.getInstance(context).getWorkInfoById(forceUploadWorkRequest!!.id)
        val runnable = Runnable{
            val workInfo = workInfoFuture.get()
            if (workInfo.state.isFinished) {
                this.forceUploadWorkRequest = null
                runBlocking(Dispatchers.Main) {
                    Toast.makeText(context, "Images were uploaded", Toast.LENGTH_SHORT)
                        .show()
                }
            } else {
                setupNextOnForceUploadWorkerChangeListener()
            }
        }
        workInfoFuture.addListener(runnable, Dispatchers.IO.asExecutor())
    }

    private fun setupPeriodicUploadWorker(): PeriodicWorkRequest {
        val constrains = Constraints.Builder()
            .setRequiredNetworkType(NetworkType.UNMETERED)
            .setRequiresBatteryNotLow(true)
            .build()

        val workRequest =
            PeriodicWorkRequestBuilder<UploadImageWorker>(Duration.of(1, ChronoUnit.HOURS))
                .setConstraints(constrains)
                .addTag(PERIODIC_UPLOAD_WORK_TAG)
                .build()

        WorkManager.getInstance(context).enqueueUniquePeriodicWork(
            PERIODIC_UPLOAD_WORK_TAG,
            ExistingPeriodicWorkPolicy.KEEP,
            workRequest
        )

        return workRequest
    }

}