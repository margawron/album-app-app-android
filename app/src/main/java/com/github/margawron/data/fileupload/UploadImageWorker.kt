package com.github.margawron.data.fileupload

import android.content.Context
import androidx.hilt.work.HiltWorker
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.github.margawron.data.fileupload.FileUploadRepository
import dagger.assisted.Assisted
import dagger.assisted.AssistedInject
import kotlinx.coroutines.runBlocking

@HiltWorker
class UploadImageWorker @AssistedInject constructor(
    private val fileUploadRepository: FileUploadRepository,
    @Assisted applicationContext: Context,
    @Assisted workerParameters: WorkerParameters
): Worker(applicationContext, workerParameters) {
    override fun doWork(): Result {
        runBlocking {
            fileUploadRepository.uploadAllFiles()
        }
        return Result.success()
    }

}