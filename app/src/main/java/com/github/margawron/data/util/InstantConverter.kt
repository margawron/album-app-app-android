package com.github.margawron.data.util

import androidx.room.TypeConverter
import java.time.Instant

class InstantConverter {

    @TypeConverter
    fun instantToLong(instant: Instant): Long = instant.toEpochMilli()

    @TypeConverter
    fun longToInstant(millis: Long): Instant = Instant.ofEpochMilli(millis)
}