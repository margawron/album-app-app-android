package com.github.margawron.databind.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.github.margawron.databinding.MediaItemBinding
import com.github.margawron.viewmodels.MediaItemViewModel

class MediaItemAdapter(
    private val items: List<MediaItemViewModel>
): RecyclerView.Adapter<MediaItemAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = MediaItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    override fun getItemCount(): Int = items.size

    inner class ViewHolder(val binding: MediaItemBinding): RecyclerView.ViewHolder(binding.root){
        fun bind(item: MediaItemViewModel){
            binding.vm = item
            item.setBinding(binding)
        }
    }
}