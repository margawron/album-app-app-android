package com.github.margawron.databind.fragments

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.github.margawron.R
import com.github.margawron.data.filechanges.FileChangesRepository
import com.github.margawron.data.fileupload.FileUploadRepository
import com.github.margawron.databind.adapters.MediaItemAdapter
import com.github.margawron.databinding.MediaViewFragmentBinding
import com.github.margawron.viewmodels.MediaItemViewModel
import com.github.margawron.viewmodels.MediaViewViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.*
import java.lang.Exception
import javax.inject.Inject

@AndroidEntryPoint
class MediaViewFragment : Fragment() {

    @Inject
    lateinit var fileChangesRepository: FileChangesRepository

    @Inject
    lateinit var fileUploadRepository: FileUploadRepository

    companion object {
        fun newInstance() = MediaViewFragment()
    }

    private val viewModel by viewModels<MediaViewViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: MediaViewFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.media_view_fragment, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.vm = viewModel

        binding.mediaRecycler.also {
            it.setHasFixedSize(false)
            it.adapter = MediaItemAdapter(listOf())
        }

        val fileChanges = fileChangesRepository.getFileChanges()
        fileChanges.observe(viewLifecycleOwner) { changes ->
            viewLifecycleOwner.lifecycleScope.launch {
                binding.mediaRecycler.also { recycler ->
                    val viewModels = withContext(Dispatchers.Default) {
                        return@withContext changes.map {
                            MediaItemViewModel(
                                it.updateInstant,
                                it.status,
                                { forceUploadImage(it.id!!) },
                                { imageView -> loadImage(imageView, Uri.parse(it.uri)) }
                            )
                        }.toList()
                    }
                    withContext(Dispatchers.Main) {
                        recycler.adapter = MediaItemAdapter(viewModels)
                    }
                }
            }
        }

        return binding.root
    }

    fun forceUploadImage(fileChangeId: Long){
        lifecycleScope.launch(Dispatchers.IO){
            fileUploadRepository.uploadFile(fileChangeId)

        }
        Toast.makeText(context, "File change id $fileChangeId", Toast.LENGTH_SHORT).show()
    }

    fun loadImage(imageView: ImageView, uri: Uri){
        try {
            Glide.with(this)
                .load(uri)
                .into(imageView)
        } catch (e: Exception){
            Log.d("loadImage", "Glide exception: ${e.message}")
        }
    }

}