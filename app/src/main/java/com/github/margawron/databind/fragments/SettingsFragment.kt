package com.github.margawron.databind.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.github.margawron.R
import com.github.margawron.data.auth.AuthManager
import com.github.margawron.data.fileupload.FileUploadRepository
import com.github.margawron.databinding.SettingsFragmentBinding
import com.github.margawron.service.MediaChangeService
import com.github.margawron.viewmodels.SettingsViewModel
import com.jakewharton.processphoenix.ProcessPhoenix
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class SettingsFragment: Fragment() {

    @Inject
    lateinit var fileUploadRepository: FileUploadRepository

    @Inject
    lateinit var authManager: AuthManager

    companion object{
        fun newInstance() = SettingsFragment()
    }

    private val viewModel = SettingsViewModel(
        { switchBackgroundService() },
        { forceUploadAllFiles() },
        { logout() }
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding: SettingsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.settings_fragment, container, false)
        binding.lifecycleOwner = this
        binding.vm = viewModel

        return binding.root
    }

    private fun forceUploadAllFiles(){
        viewLifecycleOwner.lifecycleScope.launch(Dispatchers.IO){
            fileUploadRepository.setupForceUploadWork()
        }
    }

    private fun switchBackgroundService(){
        if (MediaChangeService.instance != null) {
            val intent = Intent(requireContext(), MediaChangeService::class.java)
            requireContext().stopService(intent)
        } else {
            val intent = Intent(requireContext(), MediaChangeService::class.java)
            requireContext().startForegroundService(intent)
        }
    }

    private fun logout(){
        authManager.logout()
        switchBackgroundService()
        ProcessPhoenix.triggerRebirth(requireContext())
    }
}