package com.github.margawron.di

import android.content.Context
import com.github.margawron.data.AppDatabase
import com.github.margawron.data.filechanges.FileChangesDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideApplicationDatabase(@ApplicationContext context: Context): AppDatabase = AppDatabase.getInstance(context)

    @Singleton
    @Provides
    fun provideFileChangesDao(appDatabase: AppDatabase): FileChangesDao = appDatabase.fileChangesDao()
}