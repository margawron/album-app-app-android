package com.github.margawron.fileobserver

enum class FileChangeFlag(val flag: Int) {
    INSERT(1 shl 2),
    UPDATE(1 shl 3),
    DELETE(1 shl 4);

    companion object{
        fun parseFlags(flags: Int): Set<FileChangeFlag> =
            values()
                .filter {
                    it.flag and flags > 0
                }.toSet()
    }
}