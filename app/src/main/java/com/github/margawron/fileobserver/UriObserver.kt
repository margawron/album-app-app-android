package com.github.margawron.fileobserver

import android.database.ContentObserver
import android.net.Uri
import com.github.margawron.data.filechanges.FileChangesRepository
import com.github.margawron.data.filechanges.FileKind
import kotlinx.coroutines.*

class UriObserver(
    private val fileKind: FileKind,
    private val fileChangesRepository: FileChangesRepository,
    private val coroutineScope: CoroutineScope
) : ContentObserver(null) {

    override fun onChange(selfChange: Boolean, uri: Uri?, flags: Int) {
        val fileChangeFlags = FileChangeFlag.parseFlags(flags)
        uri?.let{ fileUri ->
            coroutineScope.launch(Dispatchers.IO) {
                fileChangesRepository.registerFileChange(fileUri, fileKind, fileChangeFlags)
            }
        }
    }
}