package com.github.margawron.service

import android.app.NotificationChannel
import android.app.NotificationManager
import androidx.core.app.NotificationCompat
import androidx.lifecycle.LifecycleService
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import com.github.margawron.R
import com.github.margawron.data.filechanges.FileChange
import com.github.margawron.data.filechanges.FileChangesRepository
import com.github.margawron.util.FormattingUtils
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject

@AndroidEntryPoint
class MediaChangeService : LifecycleService() {

    companion object{
        const val CHANNEL_ID = "mediaChange"
        var instance: MediaChangeService? = null
    }

    @Inject
    lateinit var fileChangesRepository: FileChangesRepository

    private lateinit var itemsSentLiveData: LiveData<Long>
    private lateinit var itemsPendingLiveData: LiveData<Long>
    private lateinit var latestUpdateLiveData: LiveData<FileChange?>

    override fun onCreate() {
        super.onCreate()
        instance = this
        lifecycleScope.launch {
            withContext(Dispatchers.IO){
                itemsSentLiveData = fileChangesRepository.getUploadedItemsCount()
                itemsPendingLiveData = fileChangesRepository.getPendingItemsCount()
                latestUpdateLiveData = fileChangesRepository.getLatestUpdate()
                withContext(Dispatchers.Main){
                    val notificationChannel = NotificationChannel(
                        CHANNEL_ID,
                        getString(R.string.media_listener_background_service),
                        NotificationManager.IMPORTANCE_HIGH
                    )
                    val notificationManager = getSystemService(NotificationManager::class.java)
                    notificationManager!!.createNotificationChannel(
                        notificationChannel
                    )
                    val builder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                    with(builder) {
                        setContentTitle(getString(R.string.backup_notification_title))
                        val style = createNotificationStyle(
                            latestUpdateLiveData.value,
                            itemsSentLiveData.value,
                            itemsPendingLiveData.value
                        )
                        setStyle(style)
                        setSmallIcon(R.drawable.ic_upload)
                        setOnlyAlertOnce(true)
                    }
                    val notification = builder.build()
                    startForeground(1, notification)
                    setupNotificationUpdates(builder)
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        instance = null
    }

    private fun setupNotificationUpdates(builder: NotificationCompat.Builder) {
        val notificationManager = getSystemService(NotificationManager::class.java)

        val observer = { _: Any? ->
            val style = createNotificationStyle(
                latestUpdateLiveData.value,
                itemsSentLiveData.value,
                itemsPendingLiveData.value
            )
            builder.setStyle(style)
            notificationManager.notify(1, builder.build())
        }
        itemsSentLiveData.observe(this, observer)
        itemsPendingLiveData.observe(this, observer)
        latestUpdateLiveData.observe(this, observer)
    }

    private fun createNotificationStyle(
        fileChange: FileChange?,
        itemsSentValue: Long?,
        itemsPendingValue: Long?
    ): NotificationCompat.InboxStyle {
        val lastUpdate = fileChange?.let {
            FormattingUtils.getFormattedInstant(it.updateInstant)
        } ?: getString(R.string.backup_notification_last_update_never)
        val itemsSent = itemsSentValue ?: 0
        val itemsPending = itemsPendingValue ?: 0
        return NotificationCompat.InboxStyle()
            .addLine(getString(R.string.backup_notification_last_upload).format(lastUpdate))
            .addLine(getString(R.string.backup_notification_items_pending).format(itemsPending))
            .addLine(getString(R.string.backup_notification_items_sent).format(itemsSent))
    }
}