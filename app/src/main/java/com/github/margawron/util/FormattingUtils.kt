package com.github.margawron.util

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter

object FormattingUtils {

    private val dayMonthYearHourMinuteSecondFormatter: DateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-uuuu HH:mm:ss")

    fun getFormattedInstant(instant: Instant) =
        LocalDateTime
            .ofInstant(instant, ZoneId.systemDefault())
            .format(dayMonthYearHourMinuteSecondFormatter)
}