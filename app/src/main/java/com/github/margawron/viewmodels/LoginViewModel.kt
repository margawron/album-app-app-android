package com.github.margawron.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.github.margawron.util.PreferenceHelper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject


@HiltViewModel
class LoginViewModel @Inject constructor(
    private val preferenceHelper: PreferenceHelper
): ViewModel() {
    var login: String = preferenceHelper.getLastLoggedUsername()
    var password: String = preferenceHelper.getLastLoggedPassword()
    var rememberPassword: Boolean = preferenceHelper.getShouldRememberPassword()
    var onLoginButtonClickedCallback: (CoroutineScope) -> Unit = {}

    init {
        if(preferenceHelper.getShouldRememberPassword()){
            onLoginClick()
        }
    }

    fun onLoginClick () {
        onLoginButtonClickedCallback(viewModelScope)
    }

    fun checkIfShouldRememberPassword() {
        if (rememberPassword) {
            preferenceHelper.setLastLoggedUsername(login)
            preferenceHelper.setLastLoggedPassword(password)
            preferenceHelper.setShouldRememberPassword(true)
        } else {
            preferenceHelper.setLastLoggedUsername("")
            preferenceHelper.setLastLoggedPassword("")
            preferenceHelper.setShouldRememberPassword(false)
        }
    }
}