package com.github.margawron.viewmodels

import android.view.View
import android.widget.ImageView
import androidx.lifecycle.ViewModel
import com.github.margawron.data.filechanges.FileChangeStatus
import com.github.margawron.databinding.MediaItemBinding
import java.time.Instant

class MediaItemViewModel(
    private val creationInstant: Instant,
    private val status: FileChangeStatus,
    private val onClickCallback: () -> Unit,
    private val imageLoadingCallback: (ImageView) -> Unit
): ViewModel() {


    fun setBinding(binding: MediaItemBinding){
        imageLoadingCallback(binding.mediaItemImagePreview)
        if (status == FileChangeStatus.PENDING){
            binding.mediaItemUploadButton.visibility = View.VISIBLE
        } else {
            binding.mediaItemUploadButton.visibility = View.INVISIBLE
        }
    }

    fun getUpdatedString() = creationInstant.toString()

    fun getStatus(): String =
        status.name.lowercase().replaceFirstChar {
            it.uppercase()
        }

    fun onClick() {
        onClickCallback()
    }
}