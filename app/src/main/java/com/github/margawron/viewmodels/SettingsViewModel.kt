package com.github.margawron.viewmodels

import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

class SettingsViewModel(
    private val turnOffServiceCallback: () -> Unit,
    private val forceUploadAllFilesCallback: () -> Unit,
    private val logoutCallback: () -> Unit
): ViewModel() {

    fun turnOffService(){
        turnOffServiceCallback()
    }

    fun forceUpload(){
        forceUploadAllFilesCallback()
    }

    fun logout(){
        logoutCallback()
    }
}